<?php
require('fpdf.php');
class PDF extends FPDF
{
}
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image('logocgpit.png',23,26,165,169);
$pdf->SetFont('Helvetica','U',42);
$pdf->Text(70,69,"Certificate"); 
$pdf->SetXY(22,100);
$pdf->SetLeftMargin(22);
$pdf->SetFont('Helvetica','I',15);
//$pdf->Write(8,'This is to certify that Mr/Ms. ____________________________________  Enrollment No: __________________  Exam Seat No: ______________  Of B.Tech Semester 5 th Computer Engineering satisfactory completed his/her laboratory work of Java Technologies (030090502) during the regular term in academic year 2017-18.');
$pdf->Write(8,'This is to certify that Mr/Ms. ____________________________________  Enrollment No: __________________  Exam Seat No: ______________  Of ');
$pdf->SetFont('Helvetica','BI',15);
$pdf->Write(8,'B.Tech Semester 5 th Computer Engineering ');
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'satisfactory completed his/her laboratory work of ');
$pdf->SetFont('Helvetica','BI',15);
$pdf->Write(8,'Java Technologies (030090502) ');
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'during the regular term in academic year ');
$pdf->Write(8,'2017-18.');
$pdf->SetXY(22,150);
$pdf->SetFont('Helvetica','BI',14);
$pdf->Write(8,'Date of Submission : ___/___/_____  No. of practical certified : ___/___ ');
$pdf->SetXY(93,192);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,'College Seal');
$pdf->SetXY(91,222);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,'Sign of Examiner');
$pdf->SetXY(145,222);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,'Prof. Purvi Tandel');
$pdf->SetXY(145,232);
$pdf->SetLeftMargin(145);
$pdf->SetFont('Helvetica','',11);
$pdf->Write(5,'Head (CE & IT Dept.) C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
$pdf->SetXY(22,222);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,'Prof. Purvi Tandel');
$pdf->SetXY(22,232);
$pdf->SetLeftMargin(22);
$pdf->SetRightMargin(129);
$pdf->SetFont('Helvetica','',11);
$pdf->Write(5,'Asst. Prof. (CE & IT Dept.) C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
$pdf->Output();
?>