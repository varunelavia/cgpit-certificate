<!DOCTYPE html>
<html lang="en">
<head>
  <title>Certificate</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>CGPIT Certificate</h2>
  <h4>Leave the fields empty if not sure !!!</h4>
  <form action="receiver.php" method = "POST">
    <div class="form-group">
      <label>Name:</label>
      <input type="text" class="" id="name" placeholder="Enter Name" name="name">
    </div>
    <div class="form-group">
      <label>Enrollment No:</label>
      <input type="number" class="" id="enrno" placeholder="Enter Enrollment No" name="enrno">
    </div>
    <div class="form-group">
      <label>Seat No:</label>
      <input type="number" class="" id="sno" placeholder="Enter Seat No" name="sno">
    </div>
    <div class="form-group">
      <label for="sel1">Select Course:</label>
      <select class="" name="course">
        <option value="Diploma">Diploma</option>
        <option value="B.Tech">B.Tech</option>
        <option value="M.Tech">M.Tech</option>
      </select>
    </div>
    <div class="form-group">
      <label for="sel1">Select Semester:</label>
      <select class="" name="sem">
        <option value="Semester 1st">1</option>
        <option value="Semester 2nd">2</option>
        <option value="Semester 3rd">3</option>
        <option value="Semester 4th">4</option>
        <option value="Semester 5th">5</option>
        <option value="Semester 6th">6</option>
        <option value="Semester 7th">7</option>
        <option value="Semester 8th">8</option>
      </select>
    </div>
    <div class="form-group">
      <label for="sel1">Select Field:</label>
      <select class="" name="field">
        <option value="Computer Engineering">Computer Engineering</option>
        <option value="Information Technology">Information Technology</option>
        <option value="Electronics and Communication">Electronics and Communication</option>
        <option value="Electrical Engineering">Electrical Engineering</option>
        <option value="Mechanical Engineering">Mechanical Engineering</option>
        <option value="Automobile Engineering">Automobile Engineering</option>
        <option value="Civil Engineering">Civil Engineering</option>
        <option value="Chemical Engineering">Chemical Engineering</option>
      </select>
    </div>
    <div class="form-group">
      <label for="sel1">Department:</label>
      <select class="" name="department">
        <option value="CE & IT Dept.">CE & IT Dept.</option>
        <option value="EC & EE Dept.">EC & EE Dept.</option>
        <option value="Auto & Mech Dept.">Auto & Mech Dept.</option>
        <option value="Civil Dept.">Civil Dept.</option>
        <option value="Chemical Dept.">Chemical Dept.</option>
      </select>
    </div>
    <div class="form-group">
      <label>Subject Name:</label>
      <input type="text" class="" id="subject" placeholder="Subject Name (Subject Code)" name="subject" style="width:200px;">
    </div>
    <div class="form-group">
      <label for="sel1">Date of Submission:</label>
      <input type="number" name = "dos" style="width:50px;" placeholder = "DD" min="1" max="31"> / 
      <input type="number" name = "mos" style="width:50px;" placeholder = "MM" min="1" max="12"> / 
      <input type="number" name = "yos" style="width:50px;" placeholder = "YY" min="2017" max="2099">
    </div>
    <div class="form-group">
      <label for="sel1">No of Practicals Certified:</label>
      <input type="number" name = "sp" style="width:50px;" min="1" max="50"> / 
      <input type="number" name = "tp" style="width:50px;" min="1" max="50">
    </div>
    <div class="form-group">
      <label for="sel1">Academic Year:</label>
      20 <input type="number" name = "ay1" style="width:50px;" min="17" max="99" required> - 
      20 <input type="number" name = "ay2" style="width:50px;" min="17" max="99" required>
    </div>
    <div class="form-group">
      <label>Subject Faculty Name:</label>
      <input type="text" class="" id="subjectfaculty" value="Prof " placeholder = "Prof Mr/Ms/ Faculty Name" name="subjectfaculty" required>
    </div>
    <div class="form-group">
      <label>HOD Name:</label>
      <input type="text" class="" id="subjectfaculty" value="Prof " placeholder = "Prof Mr/Ms/ Faculty Name" name="hodname" required>
    </div>
    <input type="submit" class="btn btn-default" name="submit" value="Submit">
  </form>
</div>

</body>
</html>