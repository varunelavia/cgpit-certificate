<?php
if(isset($_POST['submit']))
{
require('fpdf.php');
class PDF extends FPDF
{
}
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image('logocgpit.png',23,26,165,169);
$pdf->SetFont('Helvetica','U',42);
$pdf->Text(70,69,"Certificate"); 
$pdf->SetXY(22,100);
$pdf->SetLeftMargin(22);
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'This is to certify that Mr/Ms. ');
if($_POST['name'] == "")
$pdf->Write(8,'____________________________________   ');
else
{
$pdf->SetFont('Helvetica','BIU',15);
$pdf->Write(8,$_POST['name']);
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'  ');
}
$pdf->Write(8,'Enrollment No: ');
if($_POST['enrno'] == "")
$pdf->Write(8,'__________________  ');
else
{
$pdf->SetFont('Helvetica','BIU',15);
$pdf->Write(8,$_POST['enrno']);
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'  ');
}
$pdf->Write(8,'Exam Seat No: ');
if($_POST['sno'] == "")
$pdf->Write(8,'______________  Of ');
else
{
$pdf->SetFont('Helvetica','BIU',15);
$pdf->Write(8,$_POST['sno']);
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'  Of ');
}
$pdf->SetFont('Helvetica','BI',15);
$pdf->Write(8,$_POST['course']." ".$_POST['sem']." ".$_POST['field']." ");
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'satisfactory completed his/her laboratory work of ');
$pdf->SetFont('Helvetica','BI',15);
$pdf->Write(8,$_POST['subject'].' ');
$pdf->SetFont('Helvetica','I',15);
$pdf->Write(8,'during the regular term in academic year ');
$pdf->Write(8,'20'.$_POST['ay1'].'-20'.$_POST['ay2'].".");
$pdf->SetXY(22,150);
$pdf->SetFont('Helvetica','BI',14);
$pdf->Write(8,'Date of Submission : ');
if($_POST['dos'] == "" || $_POST['mos'] == "" || $_POST['yos'] == "" )
$pdf->Write(8,'___/___/_____  ');
else
{
$pdf->SetFont('Helvetica','BIU',15);
$pdf->Write(8,$_POST['dos']."/".$_POST['mos']."/".$_POST['yos']);
$pdf->SetFont('Helvetica','BI',15);
$pdf->Write(8,'  ');
}
$pdf->Write(8,'No. of practical certified : ');
if($_POST['sp'] == "" || $_POST['tp'] == "")
$pdf->Write(8,'___/___ ');
else
{
$pdf->SetFont('Helvetica','BIU',15);
$pdf->Write(8,$_POST['sp']."/".$_POST['tp']);
$pdf->SetFont('Helvetica','BI',15);
$pdf->Write(8,'  ');
}
$pdf->SetXY(93,192);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,'College Seal');
$pdf->SetXY(91,222);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,'Sign of Examiner');
$pdf->SetXY(141,222);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,$_POST['hodname']);
$pdf->SetXY(141,232);
$pdf->SetLeftMargin(141);
$pdf->SetFont('Helvetica','',11);
//$pdf->Write(5,'Head ('.$_POST['department'].') C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
$pdf->Write(5,'Head ('.$_POST['department'].')');
$pdf->SetXY(141,237);
$pdf->SetLeftMargin(141);
$pdf->Write(5,'C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
$pdf->SetXY(22,222);
$pdf->SetFont('Helvetica','B',11);
$pdf->Write(8,$_POST['subjectfaculty']);
$pdf->SetXY(22,232);
$pdf->SetLeftMargin(22);
$pdf->SetRightMargin(129);
$pdf->SetFont('Helvetica','',11);
if($_POST['subjectfaculty'] == $_POST['hodname'])
{
//$pdf->Write(5,'Head ('.$_POST['department'].') C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
$pdf->Write(5,'Head ('.$_POST['department'].')');
$pdf->SetXY(22,237);
$pdf->Write(5,'C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
}
else
{
//$pdf->Write(5,'Asst. Prof. ('.$_POST['department'].') C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
$pdf->Write(5,'Asst. Prof. ('.$_POST['department'].')');
$pdf->SetXY(22,237);
$pdf->Write(5,'C. G. Patel Institute of Technology, Gopal Vidhyanagar, Bardoli, Surat.');
}
$pdf->Output("D","Certificate_".$_POST['subject'].".pdf");
}
?>